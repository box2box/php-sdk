<?php

namespace box2box\php_sdk;

use Exception;
use Guzzle\Http\Client;

class Box2boxSdk
{
    /**
     * @var - Версия API
     */
    private $version;

    /**
     * @var - url API
     */
    private $baseUrl;

    private $login;

    private $password;

    private $decodeJson;

    /**
     * @var - Токен используется для подписи запросов к Api
     */
    private $token;

    /**
     * @param $url - урл, на который будем стучать
     * Например, http://api.box2box.ru
     * @param $login
     * @param $password
     * @param $version - версия API
     * @param bool $decode_json - конвертировать ли json-строку ответа
     * @throws Exception
     */
    public function __construct($base_url, $login, $password, $version = 1, $decode_json = true)
    {
        if (
            is_string($base_url) && !empty($base_url)
            && is_string($login) && !empty($login)
            && is_string($password) && !empty($password)
            && !empty($version)
        ) {
            $this->login = trim($login);
            $this->password = trim($password);
            $this->baseUrl = trim($base_url, " \t\n\r\0\x0B/");
            $this->version = trim($version);
            $this->getToken();
        } else {
            throw new Exception('Base_url, version, login and password might be a non empty string!');
        }

        $this->decodeJson = (bool)$decode_json;
    }

    private function getToken()
    {
        if (empty($this->token)) {
            $url = $this->baseUrl . '/v' . $this->version . '/users/' . $this->login . '/' . $this->password . '/token';
            $response = $this->call('GET', $url);
            $this->token = $response->result->key;
        }
    }

    /**
     * Создание или редактирование заказ (в зависимости от наличия идентификатора заказа)
     * @param array $numbers - Список номеров заказа
     * @param string $pickuppoint - ID ПВЗ
     * @param array $costs - Список стоимостей заказа в формате (оценочная и реальная)
     * @param array $dimensions - Массо-габаритные характеристики заказа (длина, ширина, высота, вес)
     * @param array $recipient - Данные о получателе заказа
     * @param array $items - Список вложимого
     * @param string $id - Идентификатор заказа (для редактирования)
     */
    public function orders(array $numbers, $pickuppoint, array $costs, array $dimensions, array $recipient, array $items, $id = null)
    {
        $url = $url = $this->baseUrl . '/v' . $this->version . '/orders' . ($id ? '/' . $id : '');
        $body = json_encode([
            'numbers' => $numbers,
            'pickuppoint' => $pickuppoint,
            'costs' => $costs,
            'dimensions' => $dimensions,
            'recipient' => $recipient,
            'items' => $items,
        ]);

        return $this->call('POST', $url, $this->decodeJson, $body);
    }

    /**
     * Возвращает информацию о публичном ПВЗ по идентификатору.
     * ИЛИ
     * Возвращает публичный список ПВЗ.
     *
     * если передан id точки, то можно указать только fields
     *
     * @param string /null $id - Идентификатор ПВЗ
     * @param string /null $fields - Список полей для вывода (разделённых запятой)
     * @param string /null $filter - Список фильтров (разделённых запятой, например "name=The name,type=the_type"). Используйте '!' для поиска строгого соответствия (name=!The name)
     * @param string /null $orderBy - Сортировать по заданному полю. Используйте префикс '-' для сортировки в обратном порядке (-name)
     * @param int /null $limit - Лимит выборки
     * @param int /null $offset - Пропустить заданное количество результатов
     */
    public function pickupPointsPublic($id = null, $fields = null, $filter = null, $orderBy = null, $limit = null, $offset = null)
    {
        $url = $url = $this->baseUrl . '/v' . $this->version . '/pickuppoints/' . ($id ? $id . '/' : '') . 'public';
        $query = [];
        if (!empty($fields)) $query['fields'] = (string)$fields;
        //если передан id точки, то можно указать только fields
        if (!$id) {
            if (!empty($filter)) $query['filter'] = (string)$filter;
            if (!empty($orderBy)) $query['orderBy'] = (string)$orderBy;
            if (!empty($limit)) $query['limit'] = (int)$limit;
            if (!empty($offset)) $query['offset'] = (int)$offset;
        }

        return $this->call('GET', $url, $this->decodeJson, null, $query);
    }

    /**
     * Возвращает историю изменения статусов заказа
     * @param string $id - Идентификатор заказа
     */
    public function ordersStatusesHistory($id)
    {
        $url = $url = $this->baseUrl . '/v' . $this->version . '/orders/' . $id . '/statuses/history';

        return $this->call('GET', $url, $this->decodeJson);
    }

    /**
     * Создает заявку на забор
     * @param array $data - Данные заявки
     * @return object
     * @throws Exception
     */
    public function requests(array $data)
    {
        if(empty($data)){
            throw new Exception('Data can not be empty!');
        }
        $url = $url = $this->baseUrl . '/v' . $this->version . '/requests';
        $body = json_encode([
            'data' => $data
        ]);

        return $this->call('POST', $url, $this->decodeJson, $body);
    }


    /** Общий метод для отправки запроса
     * @param $method - метод (GET, POST, ....)
     * @param $url - на какой url отправлять запрос
     * @param $decode_json - отдавать json-строку или конвертировать в объект
     * @param mixed $body - тело запроса (для метода POST)
     * @param array $query - параметры GET запроса
     * @return object
     * @throws Exception
     */
    private function call($method, $url, $decode_json=true, $body=null, array $query=[])
    {
        try{
            $client = new Client();
            $request = $client->createRequest($method, $url, null, $body, ['query'=>$query]);
            $request->setHeader('Authorization', $this->token);
            $request->setHeader('Content-Type', 'application/json');
            if($body){
                $request->setHeader('Content-Length', strlen($body));
            }
            $response = $request->send();
            if($response->isSuccessful()){
                if(!$response->getBody()->getContentLength()){
                    return (object)['result'=> null,  'url'=>$request->getUrl()];
                }

                $response_body = $response->getBody(true);

                if($decode_json){
                    $result = (object)['result'=>json_decode($response_body), 'url'=>$request->getUrl()];
                } else {
                    $result = (object)['result'=>$response_body, 'url'=>$request->getUrl()];
                }

                return $result;
            }

            throw new Exception($response->getMessage());
        }
        catch(Exception $e){
            if(method_exists($e, 'getResponse')){
                //нормальное описание ошибки приходи в теле ответа
                $error_respoce = $e->getResponse();
                $error_body = json_decode($error_respoce->getBody(true));
                //проверка на валидный json в теле ответа
                if(json_last_error() == JSON_ERROR_NONE){
                    throw new Exception($error_body->error->message, $error_body->error->code, $e);
                }
            }

            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getStatusesList()
    {
        return [];
    }

}
